//
//  ContactsInfo+CoreDataProperties.swift
//  BusinessInfo
//
//  Created by Hitendra on 27/11/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//
//

import Foundation
import CoreData


extension ContactsInfo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ContactsInfo> {
        return NSFetchRequest<ContactsInfo>(entityName: "ContactsInfo")
    }

    @NSManaged public var conId: String?
    @NSManaged public var name: String?
    @NSManaged public var email: String?
    @NSManaged public var address: String?
    @NSManaged public var gender: String?
    @NSManaged public var mobile: String?

}
