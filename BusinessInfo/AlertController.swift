//
//  AlertController.swift
//  EventApp
//
//  Created by Hitendra on 08/06/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import Foundation
import UIKit

class AlertController: NSObject {
    
    static let shared = AlertController()
    
    typealias handlar = ((UIAlertAction) -> Void)?
    
    func showAlert(fromVC : UIViewController, title : String , message : String, buttonTitles : [String] , handlars : [handlar]?){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        var index = 0
        for titles in buttonTitles {
            let buttonHandlar = handlars?[index]
            alert.addAction(UIAlertAction(title: titles, style: UIAlertAction.Style.default, handler:buttonHandlar))
            index = index + 1
        }
        fromVC.present(alert, animated: true, completion: nil)
    }
    

}
