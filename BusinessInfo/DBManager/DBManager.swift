//
//  DBManager.swift
//  BusinessInfo
//
//  Created by Hitendra on 27/11/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class DBManager : NSObject {
    
    static let sharedInstance = DBManager()
    
    func mainMoc() -> NSManagedObjectContext {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { fatalError("Could not convert delegate to AppDelegate") }
        return appDelegate.persistentContainer.viewContext
    }
    
    func getUser(predicate : NSPredicate) -> NSArray {
           let users = NSMutableArray()
           let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ContactsInfo")
           fetchRequest.predicate = predicate
           do {
               let results = try self.mainMoc().fetch(fetchRequest)
               for info in results {
                   users.add(info)
               }
           } catch  {
               return users
           }
           return users
       }
    
    func saveUser(_ userDict : [String : Any] , erroHandlar : (_ errorMsg : String) -> ()) {
        
        let predicate = NSPredicate(format: "conId = %@", userDict["id"] as! CVarArg)
        let user = self.getUser(predicate: predicate)
        if(user.count == 0){
            let user = NSEntityDescription.insertNewObject(forEntityName: "ContactsInfo", into: self.mainMoc()) as? ContactsInfo
            user?.conId = (userDict["id"] as? String)
            user?.name = (userDict["name"] as? String)
            user?.email = (userDict["email"] as? String)
            user?.address = (userDict["address"] as? String)
            user?.gender = (userDict["gender"] as? String)
            let phoneno = userDict["phone"] as? [String : Any]
            user?.mobile = (phoneno!["mobile"] as? String)
        }
        else{
            let user = user.firstObject! as? ContactsInfo
            user?.conId = (userDict["id"] as? String)
            user?.name = (userDict["name"] as? String)
            user?.email = (userDict["email"] as? String)
            user?.address = (userDict["address"] as? String)
            user?.gender = (userDict["gender"] as? String)
            let phoneno = userDict["phone"] as? [String : Any]
            user?.mobile = (phoneno!["mobile"] as? String)
        }
        self.saveContext()
        erroHandlar("")
    }

    func updateUser(_ studentDict : [String : Any], _ studentTemp : ContactsInfo?){
        studentTemp?.name = (studentDict["name"] as! String)
        studentTemp?.mobile = (studentDict["mobile"] as! String)
        studentTemp?.address = (studentDict["address"] as! String)
        studentTemp?.email = (studentDict["email"] as! String)
        self.saveContext()
    }
    
    
    func saveContext(){
        do {
            try self.mainMoc().save()
        } catch  {
            print("data is not saved")
        }
    }
    
    func deleteObject(_ object : NSManagedObject){
        self.mainMoc().delete(object)
        self.saveContext()
    }
    
    
}
