//
//  ActivityView.swift
//  BusinessInfo
//
//  Created by Hitendra on 27/11/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//
import UIKit

class ActivityView: NSObject {

    static let sharedInstance = ActivityView()
    var view = UIView()
    var isLoading : Bool = false
    
    func showLoading(vc : UIViewController) {
        if isLoading {return}
        else{
        let screenRect = UIScreen.main.bounds
        view = UIView.init(frame: screenRect)
        view.tag = 100
        isLoading = true
            view.backgroundColor = UIColor(red: 88/255, green: 89/255, blue: 90/255, alpha: 0.8)
//            view.alpha = 0.8
        let viewActivity = UIView.init(frame: CGRect.init(x: 0.0, y: 0.0, width: 100, height: 100))
        let activityView = UIActivityIndicatorView(style: .large)
        activityView.center = viewActivity.center
        activityView.color = UIColor(named: "colorPrimary")
        viewActivity.addSubview(activityView)
        activityView.startAnimating()
        viewActivity.layer.cornerRadius = 10.0
        viewActivity.backgroundColor = UIColor.clear
        viewActivity.center = self.view.center
        let label = UILabel.init(frame: CGRect.init(x: 0.0, y: activityView.frame.origin.y+activityView.frame.size.height + 10 , width: 100, height: 30))
        label.textAlignment = .center
        label.text = "Please Wait"
        label.backgroundColor = .clear
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 18)
        viewActivity.addSubview(label)
        view.addSubview(viewActivity)
        vc.view.addSubview(view)
        }
    }
    func hideLoading(vc : UIViewController){
        let view = vc.view.viewWithTag(100)
        isLoading = false
        view?.removeFromSuperview()
    }
}

