//
//  ContactList.swift
//  BusinessInfo
//
//  Created by Hitendra on 27/11/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import UIKit
import CoreData

class UserCell: UITableViewCell {
    
    @IBOutlet weak var lblname : UILabel!
    @IBOutlet weak var lblEmail : UILabel!
    @IBOutlet weak var lblMobile : UILabel!
    
    func configureCell(_ conInfo : ContactsInfo){
        self.lblname.text = conInfo.name
        self.lblEmail.text = conInfo.email
        self.lblMobile.text = conInfo.mobile
    }
}

class ContactList: UIViewController {

    @IBOutlet weak var tblUserList : UITableView!
    @IBOutlet weak var txtUserSearch : UITextField!

    let contactListVM = ContactListVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contactListVM.vc = self;
        tblUserList.tableFooterView = UIView()
        self.reloadUserList()
    }
    
    func reloadUserList(){
        contactListVM.searchString = ""
        contactListVM.resetUserList()
    }
    @IBAction func back(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func mapView(_ sender : UIButton){
        
    }
    
    @IBAction func searchUser(_ sender : UIButton){
        if !(self.txtUserSearch.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
          contactListVM.searchString = self.txtUserSearch.text
          contactListVM.resetUserList()
          self.txtUserSearch.resignFirstResponder()
        }
       
    }
}
extension ContactList: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sections = contactListVM.fetchUserList.sections
        let sectionInfo = sections?[section]
        return sectionInfo?.numberOfObjects ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let CellIdentifier = "UserCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? UserCell
        cell?.selectionStyle = UITableViewCell.SelectionStyle.none
        let  user = (contactListVM.fetchUserList.object(at: indexPath)) as ContactsInfo
        cell?.configureCell(user)
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150.0
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let  user = (contactListVM.fetchUserList.object(at: indexPath)) as ContactsInfo
            DBManager.sharedInstance.deleteObject(user)
            AlertController.shared.showAlert(fromVC: self, title: "Message", message: "Deleted Successfully", buttonTitles: ["Ok"],handlars:nil)
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           let  contact = (contactListVM.fetchUserList.object(at: indexPath)) as ContactsInfo
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           let contactEdit = storyboard.instantiateViewController(withIdentifier: "ContactEdit") as! ContactEdit
           contactEdit.contactInfo = contact
           self.navigationController?.pushViewController(contactEdit, animated: true)
       }
}

extension ContactList: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tblUserList.beginUpdates()
    }
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                tblUserList.insertRows(at: [indexPath], with: .automatic)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                tblUserList.deleteRows(at: [indexPath], with: .automatic)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                tblUserList.reloadRows(at: [indexPath], with: .automatic)
            }
            break;
        case .move:
            if let newIndexPath = newIndexPath {
                if let indexPath = indexPath {
                    tblUserList.deleteRows(at: [indexPath], with: .automatic)
                    tblUserList.insertRows(at: [newIndexPath], with: .automatic)
                }
            }
            break;
        default: break
        }
    }
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tblUserList.endUpdates()
    }
}

extension ContactList : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.text = ""
        contactListVM.searchString = textField.text
        contactListVM.resetUserList()
        textField.resignFirstResponder()
        return true
    }
}
