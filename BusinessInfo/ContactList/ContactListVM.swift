//
//  ContactListVM.swift
//  BusinessInfo
//
//  Created by Hitendra on 27/11/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import UIKit
import CoreData

class ContactListVM: NSObject {

    let moc = DBManager.sharedInstance.mainMoc()
    weak var vc : ContactList?

    var searchString : String? = ""
    
    func searchKeyWord() -> NSPredicate? {
        var fieldWisePredicates: [AnyHashable] = []

        if(searchString!.count > 0){
            let unidquString = self.searchString! + "*"
            let currentDatePredicate: NSPredicate! = NSPredicate(format: "name LIKE[c] %@ || email LIKE[c] %@ || mobile LIKE[c] %@", unidquString as CVarArg,unidquString as CVarArg,unidquString as CVarArg)
            fieldWisePredicates.append(currentDatePredicate)
        }
        var finalPredicate: NSPredicate? = nil
        if let aPredicates = fieldWisePredicates as? [NSPredicate] {
            finalPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: aPredicates)
        }
        return finalPredicate
    }
    
    @objc public func resetUserList(){
        fetchUserList.fetchRequest.predicate = searchKeyWord()
        do {
            try fetchUserList.performFetch()
        } catch {
            let fetchError = error as NSError
            print("Unable to Perform Fetch Request")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        vc?.tblUserList.reloadData()
    }
    
    lazy var fetchUserList: NSFetchedResultsController<ContactsInfo> = {
           
           var fetchRequest = NSFetchRequest<NSFetchRequestResult>()
           var entity = NSEntityDescription.entity(forEntityName: "ContactsInfo", in: moc)
           fetchRequest.entity = entity
           fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
           
           // Create Fetched Results Controller
           let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: nil)
           
           // Configure Fetched Results Controller
           fetchedResultsController.delegate = vc
           
           return fetchedResultsController as! NSFetchedResultsController<ContactsInfo>
       }()
    
}
