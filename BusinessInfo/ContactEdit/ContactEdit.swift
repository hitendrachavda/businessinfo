//
//  ContactEdit.swift
//  BusinessInfo
//
//  Created by Hitendra on 27/11/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import UIKit

class ContactEdit: UIViewController {

     var contactInfo : ContactsInfo?
    
    @IBOutlet weak var scrollview : UIScrollView!
    
    @IBOutlet weak var txtName : UITextField!
    @IBOutlet weak var txtMobile : UITextField!
    @IBOutlet weak var txtAddress : UITextField!
    @IBOutlet weak var txtEmail : UITextField!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(contactInfo != nil){
            self.fillContactDetail()
            
        }
        // Do any additional setup after loading the view.
    }
    func fillContactDetail(){
        self.txtName.text = self.contactInfo!.name
        self.txtMobile.text = self.contactInfo!.mobile
        self.txtAddress.text = self.contactInfo!.address
        self.txtEmail.text = self.contactInfo!.email
    }
    @IBAction func contactUpdate(_ sender : UIButton){
           
        let contactDict : [String : Any] = [
            "name" : self.txtName.text! as String,
            "mobile" : self.txtMobile.text! as String,
            "address" : self.txtAddress.text! as String,
            "email" : self.txtEmail.text! as String,
        ]
        DBManager.sharedInstance.updateUser(contactDict, contactInfo)
        AlertController.shared.showAlert(fromVC: self, title: "Message", message: "Updated Successfully", buttonTitles: ["Ok"],handlars:nil)
    
    }
    @IBAction func goBack(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
}
extension ContactEdit : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}
