//
//  MapView.swift
//  BusinessInfo
//
//  Created by Hitendra on 27/11/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import UIKit
import MapKit


struct ContactDetail {
    var coordinate: CLLocationCoordinate2D
    var phone: String!
    var name: String!
    var address: String!
    var image: UIImage!
    var website: String!
}

class MapView: UIViewController {

    @IBOutlet private var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let image = UIImage(named: "logo")
        
        let cordinate2d = CLLocationCoordinate2D(latitude: 23.0225, longitude: 72.5714)
        let contactDetial1  = ContactDetail(coordinate: cordinate2d, phone: "079 6826 8000", name: "Arvind Ltd Corporate House", address: "Arvin Mills, Naroda Rd", image: image, website: "https://www.arvind.com/")
        self.addNewLocation(contactDetial1)
        
        let cordinate2d2 = CLLocationCoordinate2D(latitude: 23.0210, longitude: 72.5710)
        let contactDetial2  = ContactDetail(coordinate: cordinate2d2, phone: "02717 666 555", name: "Zydus Cadila Healthcare Ltd", address: "Sarkhej-Bavla N.H. No. 8A, Moraiya, Gujarat 382210", image: image, website: "https://zyduscadila.com/")
        self.addNewLocation(contactDetial2)
        
        let cordinate2d3 = CLLocationCoordinate2D(latitude: 23.0211, longitude: 72.5711)
        let contactDetial3  = ContactDetail(coordinate: cordinate2d3, phone: "079 2685 2554", name: "Gateway Group", address: " B/81, Corporate House, Judges Bunglow Rd, Bodakdev,", image: image, website: "https://thegatewaycorp.com/")
        self.addNewLocation(contactDetial3)
        
        
        let cordinate2d4 = CLLocationCoordinate2D(latitude: 23.0212, longitude: 72.5712)
        let contactDetial4  = ContactDetail(coordinate: cordinate2d4, phone: "079 2550 8001", name: "Gujarat Samachar", address: " Gujarat Samachar Bhavan, Old City, Khanpur, Ahmedabad, Gujarat 380001", image: image, website: "https://www.gujaratsamachar.com/")
        self.addNewLocation(contactDetial4)
        
        let cordinate2d5 = CLLocationCoordinate2D(latitude: 23.0213, longitude: 72.5713)
        let contactDetial5  = ContactDetail(coordinate: cordinate2d5, phone: "079 3000 1067", name: "Torrent Cables", address: " Opposite Bank Of Baroda, Pelican 6th Floor, GCCI Compound, Ashram Rd, Ellisbridge, Ahmedabad, Gujarat 380009", image: image, website: "https://www.torrentpower.com")
        self.addNewLocation(contactDetial5)
        
        
        let cordinate2d6 = CLLocationCoordinate2D(latitude: 23.0213, longitude: 72.5715)
        let contactDetial6  = ContactDetail(coordinate: cordinate2d6, phone: "079 3000 1067", name: "infibeam", address: "NSI Infinium Global Pvt. Ltd. (Infibeam.com) 9th Floor, A-Wing, Gopal Palace, Nehrunagar, Ahmedabad. Gujarat. India – 380015", image: image, website: "http://www.ia.ooo/")
        self.addNewLocation(contactDetial6)
        
        let cordinate2d7 = CLLocationCoordinate2D(latitude: 23.0215, longitude: 72.5715)
        let contactDetial7  = ContactDetail(coordinate: cordinate2d7, phone: "079 4040 0400", name: "Sadbhav Engineering Limited", address: "Sadbhav House, Opposite Law Garden Police Chowki, Ellisbridge, Ahmedabad, Gujarat 380006", image: image, website: "https://www.sadbhaveng.com/")
        self.addNewLocation(contactDetial7)
        
        
        let cordinate2d8 = CLLocationCoordinate2D(latitude: 23.0215, longitude: 72.5718)
        let contactDetial8  = ContactDetail(coordinate: cordinate2d8, phone: "079 2656 3331", name: "Claris", address: "Claris Tower, Nr. Parimal Underbridge, Ellisbridge, Ahmedabad", image: image, website: "https://www.clarislifesciences.com")
        self.addNewLocation(contactDetial8)
        
        
        let cordinate2d9 = CLLocationCoordinate2D(latitude: 23.0216, longitude: 72.5717)
        let contactDetial9  = ContactDetail(coordinate: cordinate2d9, phone: "079 2656 3331", name: "Claris", address: "Claris Tower, Nr. Parimal Underbridge, Ellisbridge, Ahmedabad", image: image, website: "https://www.clarislifesciences.com")
        self.addNewLocation(contactDetial9)
        
        let cordinate2d10 = CLLocationCoordinate2D(latitude: 23.0223, longitude: 72.5711)
        let contactDetial10  = ContactDetail(coordinate: cordinate2d10, phone: "082389 25066", name: "Intas Pharmaceuticals Limited", address: "Ghatlodiya, Ahmedabad, Gujarat 380061", image: image, website: "https://intaspharma.com/")
        self.addNewLocation(contactDetial10)
    }

    func addNewLocation(_ contact : ContactDetail){
        self.mapView.delegate = self
        let coordinate = contact.coordinate
        let point = UserAnnotation(coordinate: CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude))
        point.image = UIImage(named: "user")
        point.name = contact.name
        point.address = contact.address
        point.phone = contact.phone
        point.website = contact.website
        self.mapView.addAnnotation(point)
        let centerCoordinate = CLLocationCoordinate2D(latitude: 23.0225, longitude: 72.5714)
        
        self.mapView.setRegion(MKCoordinateRegion(center: centerCoordinate, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)), animated: true)
    }
    
    @IBAction func back(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    @objc func buttonAction(_ sender:UIButton) {
        let urlString = sender.titleLabel?.text
        if let url = URL(string: urlString!) {
            UIApplication.shared.open(url)
        }
    }
    @objc func buttonAction2(_ sender:UIButton) {
        let phoneno = sender.titleLabel?.text
        if let url = NSURL(string: "tel://\(phoneno!)") {
            UIApplication.shared.open(url as URL)
        }
    }
   
    
}


extension MapView : MKMapViewDelegate{
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation
        {
            return nil
        }
        var annotationView = self.mapView.dequeueReusableAnnotationView(withIdentifier: "Pin")
        if annotationView == nil{
            annotationView = AnnotationView(annotation: annotation, reuseIdentifier: "Pin")
            annotationView?.canShowCallout = false
        }else{
            annotationView?.annotation = annotation
        }
        annotationView?.image = UIImage(named: "shape1")
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView,
                 didSelect view: MKAnnotationView)
    {
        // 1
        if view.annotation is MKUserLocation
        {
            // Don't proceed with custom callout
            return
        }
        // 2
        let userannotation = view.annotation as! UserAnnotation
        let views = Bundle.main.loadNibNamed("CustomCalloutView", owner: nil, options: nil)
        let calloutView = views?[0] as! CustomCalloutView
        calloutView.name.text = userannotation.name
        calloutView.address.text = userannotation.address
        calloutView.website.setTitle(userannotation.website, for: .normal)
        calloutView.website.addTarget(self, action: #selector(
            MapView.buttonAction(_:)),
                                      for:.touchUpInside)
        calloutView.phoneno.addTarget(self, action: #selector(
                   MapView.buttonAction2(_:)),
                                             for:.touchUpInside)
        calloutView.phoneno.setTitle(userannotation.phone, for: .normal)
        let button = UIButton(frame: calloutView.phoneno.frame)
        calloutView.addSubview(button)
        calloutView.center = CGPoint(x: view.bounds.size.width / 2, y: -calloutView.bounds.size.height*0.52)
        view.addSubview(calloutView)
        mapView.setCenter((view.annotation?.coordinate)!, animated: true)
    }
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if view.isKind(of: AnnotationView.self)
        {
            for subview in view.subviews
            {
                subview.removeFromSuperview()
            }
        }
    }
    
}
