//
//  ViewController.swift
//  BusinessInfo
//
//  Created by Hitendra on 27/11/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func getData(_ sender : UIButton){
        ActivityView.sharedInstance.showLoading(vc: self)
        WebServiceManager.sharead.loadUsers(urlString: "https://api.androidhive.info/contacts/") { (response, error) in
            ActivityView.sharedInstance.hideLoading(vc: self)
            AlertController.shared.showAlert(fromVC: self, title: "Message", message: "Inserted Successfully", buttonTitles: ["Ok"],handlars:nil)
            if(error == nil){
                let userList = response!["contacts"] as! [[String : Any]]
                for userInfo in userList {
                    DBManager.sharedInstance.saveUser(userInfo) { (erroMsg) in
                    }
                }
            }
            else{
                print(error?.localizedDescription as Any)
            }
        }
    }
}

